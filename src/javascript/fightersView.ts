import View from "./view";
import FighterView from "./fighterView";
import Fight from "./Fight";
import Modal from "./modal";

class FightersView extends View {
  constructor(fighters) {
    super();

    this._handleFighterClick = this.handleFighterClick.bind(this);
    this._pickFighter = this.pickFighter.bind(this);
    this.createFighters(fighters);
  }

  currentFighter = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(
        fighter,
        this._handleFighterClick,
        this._pickFighter
      );
      return fighterView.element;
    });

    this.element = this.createElement({
      tagName: "div",
      className: "fighters"
    });

    const startBtn = this.createElement({
      tagName: "button",
      className: "startFight"
    });
    startBtn.innerText = "Start fight";
    const prepareFight = new Fight();
    startBtn.addEventListener("click", () => {
      prepareFight.prepareToFight(this.currentFighter, this.element, startBtn);
    });
    const btnWrapper = document.createElement("div");
    btnWrapper.classList.add("btnWrapper");
    btnWrapper.append(startBtn);

    this.element.appendChild(btnWrapper);
    this.element.append(...fighterElements);
  }

  pickFighter(fighter, el) {
    let isset = this.currentFighter.has(fighter._id);

    if (isset) {
      this.currentFighter.delete(fighter._id);
      el.classList.remove("visible");

      const a = el.querySelector("button");
      a.innerText = "Choose hero";
    }
    if (this.currentFighter.size < 2 && !isset) {
      this.currentFighter.set(fighter._id, fighter);

      el.classList.add("visible");
      const a = el.querySelector("button");
      a.innerText = "Choose another hero";
    }
  }

  handleFighterClick(fighter) {
    const modal = new Modal(fighter);

    this.element.appendChild(modal.element);
  }
}

export default FightersView;
