import View from "./view";

class FighterView extends View {
  constructor(fighter, _handleFighterClick, _pickFighter) {
    super();

    this.createFighter(fighter, _handleFighterClick, _pickFighter);
  }

  createFighter(fighter, _handleFighterClick, _pickFighter) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const btnElement = this.createBtn("Choose hero");

    btnElement.addEventListener("click", event => {
      event.stopPropagation();

      _pickFighter(fighter, this.element);
    });

    this.element = this.createElement({
      tagName: "div",
      className: "fighter",
      attributes: {
        id: fighter._id
      }
    });
    this.element.addEventListener("click", () => _handleFighterClick(fighter));
    this.element.append(imageElement, nameElement, btnElement);
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: "span",
      className: "name"
    });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: "img",
      className: "fighter-image",
      attributes
    });

    return imgElement;
  }

  createBtn(text) {
    const btnElement = this.createElement({
      tagName: "button",
      className: "fighter-button"
    });
    btnElement.innerText = text;

    return btnElement;
  }
}

export default FighterView;
