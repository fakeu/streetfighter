import View from "./view";

export default class Modal extends View {
  constructor(fighter) {
    super();

    this.createModal(fighter);
  }

  createModal(fighter: Object) {
    const { _id, name, health, attack, defense } = fighter;
    const heading = this.createElement({
      tagName: "h1",
      className: "modalHeading"
    });
    const closeBtn = this.createElement({
      tagName: "button",
      className: "closeBtn"
    });
    const healthField = this.createElement({
      tagName: "div",
      className: "infoField"
    });
    const attackField = this.createElement({
      tagName: "div",
      className: "infoField"
    });
    const defenseField = this.createElement({
      tagName: "div",
      className: "infoField"
    });
    const modalBody = this.createElement({
      tagName: "div",
      className: "modalBody"
    });

    heading.innerText = name;

    closeBtn.innerText = "close";
    closeBtn.addEventListener("click", () =>
      this.element.parentNode.removeChild(this.element: HTMLElement)
    );

    healthField.innerText = `Health = ${health}`;
    attackField.innerText = `Attack = ${attack}`;
    defenseField.innerText = `Defense = ${defense}`;

    modalBody.addEventListener("click", event => event.stopPropagation());
    modalBody.append(closeBtn, heading, healthField, attackField, defenseField);

    this.element = this.createElement({
      tagName: "div",
      className: "modalWrapper"
    });

    this.element.addEventListener("click", () =>
      this.element.parentNode.removeChild(this.element)
    );

    this.element.append(modalBody);
  }
}
