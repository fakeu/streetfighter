interface IFighter {
  name: string;
  health: number;
  attack: number;
  defense: number;
  dead: boolean;
}
export default class Fighter implements IFighter {
  name: string;
  health: number;
  attack: number;
  defense: number;
  dead: boolean;
  constructor(fighter: IFighter) {
    this.name = fighter.name;
    this.health = fighter.health;
    this.attack = fighter.attack;
    this.defense = fighter.defense;
    this.dead = false;
  }

  getHitPower() {
    return this.attack * Math.floor(Math.random() * (3 - 1) + 1);
  }

  getBlockPower() {
    const block = this.defense * Math.floor(Math.random() * (3 - 1) + 1);
    return block;
  }

  hit(damage: number) {
    const hit =
      damage > this.getBlockPower() ? damage - this.getBlockPower() : 0;

    this.health > 0 ? (this.health = this.health - hit) : (this.dead = true);
  }
}
