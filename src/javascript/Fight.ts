import Fighter from "./Fighter";

export default class Fight {
  constructor(hero, enemy) {}

  round: number = 1;
  fightStatus = false;

  prepareToFight(fighterList, elements, btn) {
    if (!this.fightStatus) {
      if (fighterList.size < 2) {
        btn.innerText = "pick 2 heroes";
      } else {
        const fighterElements = elements.querySelectorAll(".fighter");
        fighterElements.forEach(el => {
          el.classList.add("hidden");
        });
        const arr = [];
        fighterList.forEach(el => {
          arr.push(new Fighter(el));
        });
        this.startFight(...arr);
        btn.innerText = "reload";
        this.fightStatus = true;
      }
    } else {
      location.reload();
    }
  }

  startFight(hero, enemy) {
    const rec = () => {
      if (!hero.dead && !enemy.dead) {
        if (this.round % 2 === 0) {
          hero.hit(enemy.getHitPower());
          setTimeout(() => {
            console.log(`${enemy.name} = ${enemy.health}`);
            rec();
          }, 500);
        } else {
          enemy.hit(hero.getHitPower());
          setTimeout(() => {
            console.log(`${hero.name} = ${hero.health}`);
            rec();
          }, 500);
        }
        this.round++;
      } else {
        const winner = hero.dead
          ? `${enemy.name} Winner`
          : `${hero.name} Winner`;
        console.log(winner);
      }
    };

    rec();
  }
}
