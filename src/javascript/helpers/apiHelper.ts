const API_URL: string = "https://street-fighter-serv.herokuapp.com/api/hero/";

function callApi(method: string, endpoind: string) {
  const url: string = API_URL + endpoind;
  const options = {
    method
  };

  return fetch(url, options)
    .then(response =>
      response.ok ? response.json() : Promise.reject(Error("Failed to load"))
    )
    .catch(error => {
      throw error;
    });
}

export { callApi };
