import { callApi } from "../helpers/apiHelper";

class FighterService {
  public async getFighters() {
    try {
      return await callApi("GET", "");
    } catch (error) {
      throw error;
    }
  }

  public async getFighterDetails(_id: string) {
    try {
      return await callApi("GET", _id);
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
